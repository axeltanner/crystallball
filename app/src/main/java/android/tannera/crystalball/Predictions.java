package android.tannera.crystalball;

public class Predictions {

    private static Predictions predictions;
    private String[] answers;

    private Predictions()  {
        answers = new String[] {
                "It is certain",
                "without a doubt",
                "As I see it, yes",
                "Most likely",
                "Ask again later",
                "Reply hazy try again",
                "Don't count on it",
                "My reply is no",
                "very doubtful",
                "NO"
        };
    }

    public static Predictions get() {
        if(predictions == null) {
            predictions = new Predictions();
        }
        return predictions;
    }


    public String getPrediction() {
        int random = (int )(Math. random() * 10 + 1);
        return answers[random];
    }
}
